/*!*******************************************************************************************
 *  \file      behavior_tree_editor_view.h
 *  \brief     Behavior tree Design View definition file.
 *  \details   Layout for the Behavior tree Design component. Widgets can be dynamically added to it.
 *  \author    Daniel Del Olmo
********************************************************************************************/

#ifndef BEHAVIOR_TREE_EDITOR_VIEW_H
#define BEHAVIOR_TREE_EDITOR_VIEW_H

#include <ros/ros.h>


#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QRect>
#include <QGuiApplication>
#include <QScreen>
#include <QProcess>
#include <QCloseEvent>
#include <QtTest/QtTest>


#include "behavior_tree_editor.h"
#include "ui_behavior_tree_design_view.h"

#include <thread>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "fstream"
namespace Ui {
class BehaviorTreeDesignView;
}

class BehaviorTreeDesign;

class BehaviorTreeDesignView : public QWidget
{
    Q_OBJECT

public:
    //Constructor & Destructor
    explicit BehaviorTreeDesignView(int argc, char** argv, QWidget *parent = 0);
    ~BehaviorTreeDesignView();

public:
    /*!********************************************************************************************************************
  *  \brief      This method is executed when the user clicks the cancel or accept button within the edition mode.
  **********************************************************************************************************************/
    void closeWindow();
    BehaviorTreeDesign *getBehaviorTreeDesign();

    int position_x;
    int position_y;
private:
    Ui::BehaviorTreeDesignView *ui;
    BehaviorTreeDesign *behavior_tree_editor;
    ros::NodeHandle n;
 
    std::string drone_id_namespace;
    boost::property_tree::ptree root;
    
    /*!********************************************************************************************************************
  *  \brief      This method is the responsible for seting up connections.
  *********************************************************************************************************************/
    void setUp();

    /*!************************************************************************
   *  \brief  Kills the process
   ***************************************************************************/
    void killMe();

    void setWidgetDimensions();

public Q_SLOTS:
    void CloseMessageBoxes();
    void CloseContextMenu();


};

#endif // BEHAVIOR_TREE_EDITOR_VIEW_H
