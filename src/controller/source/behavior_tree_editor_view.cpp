/*
  BehaviorTreeEditorView
  @author  Daniel Del Olmo
  @date    03-2019
  @version 3.0
*/

#include "../include/behavior_tree_editor_view.h"
#include "../include/global.h"

BehaviorTreeDesignView::BehaviorTreeDesignView(int argc, char** argv, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BehaviorTreeDesignView)
{

    //window always on top
    setWindowIcon(QIcon(":/img/img/design_behavior_tree_icon.png"));
    setWindowTitle("Behavior Tree Editor");
    Qt::WindowFlags flags = windowFlags();
    setWindowFlags(flags | Qt::WindowStaysOnTopHint);

    ui->setupUi(this); //connects all ui's triggers

    //Add the behavior tree design widget.
    behavior_tree_editor = new BehaviorTreeDesign(this);
    ui->gridLayout->addWidget(behavior_tree_editor);
    position_x=-720;
    position_y=-395;
    //Settings Widget
    setWidgetDimensions();

    //Establishment of connections
    setUp();
}

/*------------------------------------------------------------
--------------------------------------------------------------
                      Destructor
--------------------------------------------------------------  
------------------------------------------------------------*/
BehaviorTreeDesignView::~BehaviorTreeDesignView()
{
    delete ui;
    delete behavior_tree_editor;
}

/*------------------------------------------------------------
--------------------------------------------------------------
                Getters and setters
--------------------------------------------------------------
------------------------------------------------------------*/
BehaviorTreeDesign* BehaviorTreeDesignView::getBehaviorTreeDesign()
{
    return behavior_tree_editor;
}

void BehaviorTreeDesignView::setWidgetDimensions(){
    namespace pt = boost::property_tree;

    //std::string layout_dir=std::getenv("AEROSTACK_STACK")+std::string("/stack/ground_control_system/graphical_user_interface/layouts/layout.json");

    //pt::read_json(layout_dir, root);

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();

    int y0 = screenGeometry.height()/2;
    int x0 = screenGeometry.width()/2;
    int height = 790;//root.get<int>("BEHAVIOR_TREE_EDITOR.height");
    int width= 750;//root.get<int>("BEHAVIOR_TREE_EDITOR.width");

    this->resize(width,height);
    this->move(x0+ position_x/*root.get<int>("BEHAVIOR_TREE_EDITOR.position.x")*/,y0+ position_y/*root.get<int>("BEHAVIOR_TREE_EDITOR.position.y")*/);
}

void BehaviorTreeDesignView::setUp()
{
    //Nodes

    n.param<std::string>("robot_namespace", drone_id_namespace, "drone1");


}

/*------------------------------------------------------------
--------------------------------------------------------------
                Handlers for the main widget
--------------------------------------------------------------
------------------------------------------------------------*/
void BehaviorTreeDesignView::CloseMessageBoxes()
{
    QWidgetList topWidgets = QApplication::topLevelWidgets();
    for (QWidget *w: topWidgets) {
        if (QMessageBox *mb = qobject_cast<QMessageBox *>(w)) {
            QTest::keyClick(mb, Qt::Key_Enter);
        }
    }
}

void BehaviorTreeDesignView::CloseContextMenu()
{
    //QPoint p(1, 2);
    //QTest::mouseClick(this, Qt::LeftButton, Qt::NoModifier, p);
    //QTest::keyClick(this, Qt::Key_Escape);
    this->getBehaviorTreeDesign()->getBehaviorTree()->contextMenu->close();
}

void BehaviorTreeDesignView::closeWindow()
{
    //windowClosedMsgs.id = aerostack_msgs::WindowIdentifier::BEHAVIOR_TREE_DESIGN;
    //window_closed_publ.publish(windowClosedMsgs);
    this->close();
}


void BehaviorTreeDesignView::killMe()
{
#ifdef Q_OS_WIN
    enum { ExitCode = 0 };
    ::TerminateProcess(::GetCurrentProcess(), ExitCode);
#else
    qint64 pid = QCoreApplication::applicationPid();
    QProcess::startDetached("kill -9 " + QString::number(pid));
#endif // Q_OS_WIN
}


