#include <gtest/gtest.h>
#include <Qt>
#include <QMessageBox>
#include <ros/ros.h>
#include "behavior_tree_editor_view.h"
#include <cstdio>
#include <QApplication>
#include "../include/global.h"
#include <QKeyEvent>
#include <QEvent>
#include <QMouseEvent>
#include <iostream>
#include <QTimer>
#include <QtTest/QtTest>


QApplication* app;
BehaviorTreeDesignView* w;
MissionSpecificationDialog *mission_dialog;
BehaviorDialog *behavior_dialog;
int ar;
bool finished=false;
int total_subtests=0;
int passed_subtests=0;


void spinnerThread(){
    while(!finished){
        ros::spinOnce();
    }

}

void displaySubtestResults(){
    std::cout << "\033[1;33m TOTAL SUBTESTS: "<<total_subtests<<"\033[0m"<<std::endl;
    std::cout << "\033[1;33m SUBTESTS PASSED: "<<passed_subtests<<"\033[0m"<<std::endl;

    std::cout << "\033[1;33m % PASSED: "<<(passed_subtests*100.0)/(total_subtests*1.0)<<"\033[0m"<<std::endl;

}


int treeCount(QTreeWidget *tree, QTreeWidgetItem *parent = 0)
{
    int count = 0;
    if (parent == 0) {
        int topCount = tree->topLevelItemCount();
        for (int i = 0; i < topCount; i++) {
            QTreeWidgetItem *item = tree->topLevelItem(i);
            if (item->isExpanded()) {
                count += treeCount(tree, item);
            }
        }
        count += topCount;
    } else {
        int childCount = parent->childCount();
        for (int i = 0; i < childCount; i++) {
            QTreeWidgetItem *item = parent->child(i);
            if (item->isExpanded()) {
                count += treeCount(tree, item);
            }
        }
        count += childCount;
    }
    return count;
}



TEST(BehaviorTreeEditorTest, editorInitialization)
{
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    EXPECT_TRUE(treeCount(w->getBehaviorTreeDesign()->getBehaviorTree())==0);
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, createNewMission)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    QTest::mouseClick(w->getBehaviorTreeDesign()->new_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    EXPECT_TRUE(mapa.find("Ventana de mision") != mapa.end() && mapa["Ventana de mision"]=="activa");
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, cancelMission)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QTest::mouseClick(mission_dialog->ui->cancel_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    EXPECT_TRUE(mapa.find("Ventana de mision") != mapa.end() && mapa["Ventana de mision"]=="inactiva");
    w->~BehaviorTreeDesignView();
}




TEST(BehaviorTreeEditorTest, missionWithoutName)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QTimer *timer = new QTimer(w);
    QObject::connect(timer, SIGNAL(timeout()), w, SLOT(CloseMessageBoxes()));
    timer->start(1000);
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    QObject::disconnect(timer, SIGNAL(timeout()), w, SLOT(CloseMessageBoxes()));
    EXPECT_TRUE(mapa.find("Error en mision") != mapa.end() && mapa["Error en mision"]=="activa");
    mission_dialog->~MissionSpecificationDialog();
    w->~BehaviorTreeDesignView();
}


TEST(BehaviorTreeEditorTest, missionCreated)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    EXPECT_TRUE(mapa["mision creada"] == "activa" && treeCount(w->getBehaviorTreeDesign()->getBehaviorTree())!=0);
    mission_dialog->~MissionSpecificationDialog();
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, contextMenu)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    BehaviorTree* tree = w->getBehaviorTreeDesign()->getBehaviorTree();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), tree, SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    TreeItem *root =tree->getRoot();
    QRect viewport_relative_rect = tree->visualItemRect(root);
    QPoint position = viewport_relative_rect.center();
    QTimer *timer = new QTimer(w);
    QObject::connect(timer, SIGNAL(timeout()), w, SLOT(CloseContextMenu()));
    timer->start(1000);
    //QTest::mouseClick(tree->viewport(), Qt::RightButton, Qt::NoModifier, position, 100);
    //w->getBehaviorTreeDesign()->getBehaviorTree()->onCustomContextMenu(position);
    Q_EMIT(w->getBehaviorTreeDesign()->getBehaviorTree()->customContextMenuRequested(position));
    EXPECT_TRUE(mapa["menu contextual"] == "activa");
    mission_dialog->~MissionSpecificationDialog();
    w->~BehaviorTreeDesignView();

//    QPoint p(1, 2);
//    QTest::mouseClick(w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot(), Qt::RightButton, Qt::NoModifier, p);

}


TEST(BehaviorTreeEditorTest, addChild)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot());
    QString texto = QString("BEHAVIOR");
    int index = behavior_dialog->ui->node_type_combobox->findText(texto);
    behavior_dialog->ui->node_type_combobox->setCurrentIndex(index);
    index = behavior_dialog->ui->behavior_mode_combobox->findText("EXECUTE_BEHAVIOR");
    behavior_dialog->ui->behavior_mode_combobox->setCurrentIndex(index);
    index = behavior_dialog->ui->behavior_combobox->findText("TAKE_OFF");
    behavior_dialog->ui->behavior_combobox->setCurrentIndex(index);
    behavior_dialog->ui->name_content->setText("Take off");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));

    EXPECT_TRUE(mapa["hijo creado"] == "activa" && treeCount(w->getBehaviorTreeDesign()->getBehaviorTree())!=0);
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, changeNodeName)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot());
    QString texto = QString("BEHAVIOR");
    int index = behavior_dialog->ui->node_type_combobox->findText(texto);
    behavior_dialog->ui->node_type_combobox->setCurrentIndex(index);
    index = behavior_dialog->ui->behavior_mode_combobox->findText("EXECUTE_BEHAVIOR");
    behavior_dialog->ui->behavior_mode_combobox->setCurrentIndex(index);
    index = behavior_dialog->ui->behavior_combobox->findText("TAKE_OFF");
    behavior_dialog->ui->behavior_combobox->setCurrentIndex(index);
    behavior_dialog->ui->name_content->setText("Take off");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    behavior_dialog->~BehaviorDialog();
    TreeItem *item = w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot();
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->child(0));
    behavior_dialog->ui->name_content->setText("Taking off");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    EXPECT_TRUE(w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->child(0)->node_name == "Taking off");
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, addSequenceNode)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot());
    QString texto = QString("SEQUENCE");
    int index = behavior_dialog->ui->node_type_combobox->findText(texto);
    behavior_dialog->ui->node_type_combobox->setCurrentIndex(index);
    behavior_dialog->ui->name_content->setText("Secuence 1");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));

    EXPECT_TRUE(mapa["hijo creado"] == "activa" && w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->childCount() == 1);
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, addSequenceChild)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot());
    QString texto = QString("SEQUENCE");
    int index = behavior_dialog->ui->node_type_combobox->findText(texto);
    behavior_dialog->ui->node_type_combobox->setCurrentIndex(index);
    behavior_dialog->ui->name_content->setText("Secuence 1");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    mapa = {{"",""}};
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->child(0));
    texto = QString("BEHAVIOR");
    index = behavior_dialog->ui->node_type_combobox->findText(texto);
    behavior_dialog->ui->node_type_combobox->setCurrentIndex(index);
    index = behavior_dialog->ui->behavior_mode_combobox->findText("EXECUTE_BEHAVIOR");
    behavior_dialog->ui->behavior_mode_combobox->setCurrentIndex(index);
    index = behavior_dialog->ui->behavior_combobox->findText("LAND");
    behavior_dialog->ui->behavior_combobox->setCurrentIndex(index);
    behavior_dialog->ui->name_content->setText("Land");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));

    EXPECT_TRUE(mapa["hijo creado"] == "activa" && w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->child(0)->childCount()==1);
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, deleteNode)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    behavior_dialog = new BehaviorDialog(w->getBehaviorTreeDesign()->getBehaviorTree(), w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot());
    QString texto = QString("SEQUENCE");
    int index = behavior_dialog->ui->node_type_combobox->findText(texto);
    behavior_dialog->ui->node_type_combobox->setCurrentIndex(index);
    behavior_dialog->ui->name_content->setText("Secuence 1");
    QTest::mouseClick(behavior_dialog->ui->accept_button, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    TreeItem *item = w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->child(0);
    item->removeItemWidget();

    EXPECT_TRUE(w->getBehaviorTreeDesign()->getBehaviorTree()->getRoot()->childCount() == 0);
    w->~BehaviorTreeDesignView();
}

TEST(BehaviorTreeEditorTest, collapseText)
{
    mapa = {{"",""}};
    w= new BehaviorTreeDesignView (0, nullptr);
    w->show();
    mission_dialog = new MissionSpecificationDialog();
    QObject::connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), w->getBehaviorTreeDesign()->getBehaviorTree(), SLOT(createMission(const QString &)));
    mission_dialog->ui->line_edit_name->setText("mission1");
    QTest::mouseClick(mission_dialog->ui->accept_pushbutton, Qt::LeftButton, Qt::NoModifier, QPoint(1,2));
    w->getBehaviorTreeDesign()->expand_text_button->setChecked(true);
    EXPECT_TRUE(mapa["collapse text"] == "activa");
    w->~BehaviorTreeDesignView();
}




int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, ros::this_node::getName());


    system("xfce4-terminal  \--tab --title \"Behavior Coordinator\"  --command \"bash -c 'roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
           behavior_catalog_path:=${AEROSTACK_STACK}/stack/ground_control_system/graphical_user_interface/behavior_tree_editor_process/src/controller/test/behavior_catalog.yaml  \
           my_stack_directory:=${AEROSTACK_STACK}/stack/ground_control_system/graphical_user_interface/behavior_tree_editor_process; exec bash'\" &");
    ar=argc;
    app= new QApplication (argc, nullptr);

    std::thread thr(&spinnerThread);

    return RUN_ALL_TESTS();
    thr.join();


}
